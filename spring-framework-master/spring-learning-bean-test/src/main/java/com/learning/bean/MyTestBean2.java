package com.learning.bean;

public class MyTestBean2 {
	private String testStr = "testStr in MyTestBean2";

	public MyTestBean2() {
		System.out.println("Hello world. I am constructing MyTestBean2.");
	}

	public String getTestStr() {
		return testStr;
	}

	public void setTestStr(String testStr) {
		this.testStr = testStr;
	}
}
