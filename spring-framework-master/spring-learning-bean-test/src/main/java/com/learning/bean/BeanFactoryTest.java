package com.learning.bean;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@SuppressWarnings("depreccation")
public class BeanFactoryTest {
	public static void main(String[] args) {

		Resource rs = new ClassPathResource("MyTestBean.xml");
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		new XmlBeanDefinitionReader(beanFactory).loadBeanDefinitions(rs);
//		MyTestBean myTestBean = (MyTestBean)beanFactory.getBean("myTestBean");
//		System.out.println(myTestBean.getTestStr());
//		MyTestBean2 myTestBean2 = (MyTestBean2)beanFactory.getBean("myTestBean2");
//		System.out.println(myTestBean2.getTestStr());


		BeanHasProperty beanHasProperty = (BeanHasProperty)beanFactory.getBean("beanHasProperty");
	}
}
