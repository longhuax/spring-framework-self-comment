package com.learning.bean;

public class MyTestBean {
	private String testStr = "testStr";

	public MyTestBean() {
		System.out.println("Hello world. I am constructing.");
	}

	public String getTestStr() {
		return testStr;
	}

	public void setTestStr(String testStr) {
		this.testStr = testStr;
	}
}
