package com.spring.hello.world;

import com.spring.hello.world.aopdemo.TestBean;
import com.spring.hello.world.bean.factorybeanDemos.Car;
import com.spring.hello.world.customconfig.CustomUser;
import com.spring.hello.world.messageexample.MessageService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
	public static void main(String[] args) {
		// 用我们的配置文件来启动一个 ApplicationContext
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application.xml");

		System.out.println("context start success");

		// 从 context 中取出我们的 Bean，而不是用 new MessageServiceImpl() 这种方式
		MessageService messageService = context.getBean(MessageService.class);
		// 这句将输出: hello world
		System.out.println(messageService.getMessage());

		CustomUser customUser = (CustomUser) context.getBean("customUser");
		System.out.println(customUser.getUsername());

		Car car = (Car) context.getBean("car");
		System.out.printf("car initiate by FactoryBean. brand: %s,max speed: %d,price: %s%n", car.getBrand(), car.getMaxSpeed(), car.getPrice());

		System.out.printf( context.getBean("foo").toString());
		System.out.println();
		System.out.println("====================================AOP==============================================");
		TestBean testBean = (TestBean) context.getBean("test");
		testBean.test();
	}
}
