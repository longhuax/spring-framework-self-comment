package com.spring.hello.world.aopdemo.proxyDemo1;

import java.lang.reflect.Proxy;

public class DynamicProxyTest {
	public static void main(String[] args) {
		UserDao userDao = new UserDao();
		DynamicProxy dynamicProxy = new DynamicProxy(userDao);

		publicInterface proxyInstance = (publicInterface) Proxy.newProxyInstance(UserDao.class.getClassLoader(),
				userDao.getClass().getInterfaces(),
				dynamicProxy);
		proxyInstance.save();
		System.out.println("--------------------------");
		proxyInstance.find();
	}
}
