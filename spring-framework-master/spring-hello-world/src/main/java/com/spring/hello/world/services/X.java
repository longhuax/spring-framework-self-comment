package com.spring.hello.world.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class X {
//	@Autowired  // comment when test BeanFactoryPostProcessor
	public Y y;

	public X() {
		System.out.println("X created");
	}

	public void setY(Y y) {
		this.y = y;
	}
}
