package com.spring.hello.world.messageexample;

public class MessageServiceImpl implements MessageService {
	public MessageServiceImpl() {
		System.out.println("MessageServiceImpl created");
	}
	public String getMessage() {
		return "hello world";
	}
}
