package com.spring.hello.world.aopdemo.LTWDemo2;

import org.springframework.stereotype.Component;

@Component("entitlementCalculationService")
public class StubEntitlementCalculationService implements EntitlementCalculationService {
	@Override
	public void calculateEntitlement() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("----------calculateEntitlement-------------");
	}
}


