package com.spring.hello.world.aopdemo.proxyDemo1;

import org.springframework.cglib.proxy.Enhancer;


/**
 * 在设置回调enhancer.setCallback的时候需要传入MethodInterceptor的实例
 * 并且final类无法进行代理
 * */
public class CGLibProxyTest {
	public static void main(String[] args) {
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(CGLibProxy.class);
		enhancer.setCallback(new CGLibProxyInterceptor());
		CGLibProxy cgLibProxy = (CGLibProxy) enhancer.create();
		cgLibProxy.hello();
		cgLibProxy.SayOthers("final method");
	}
}
