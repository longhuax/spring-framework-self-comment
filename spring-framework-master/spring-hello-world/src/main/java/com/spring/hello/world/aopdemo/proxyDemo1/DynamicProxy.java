package com.spring.hello.world.aopdemo.proxyDemo1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DynamicProxy implements InvocationHandler {
	private UserDao userDao = null;

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("JDK Dynamic proxy begins");
		method.invoke(userDao, args);
		System.out.println("JDK Dynamic proxy ends");
		return null;
	}

	public DynamicProxy(UserDao userDao) {
		this.userDao = userDao;
	}
}
