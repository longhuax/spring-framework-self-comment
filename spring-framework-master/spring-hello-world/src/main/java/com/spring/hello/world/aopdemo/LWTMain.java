package com.spring.hello.world.aopdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LWTMain {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
		TestBean bean = (TestBean) applicationContext.getBean("test");
		bean.test();
	}
}
