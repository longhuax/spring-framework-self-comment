package com.spring.hello.world.bean.denpendencies;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CircleBeanPostProcessor implements BeanPostProcessor {
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean instanceof BeanA ? new BeanA() : bean;
	}
}
