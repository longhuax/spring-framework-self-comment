package com.spring.hello.world.util;

import com.spring.hello.world.services.Y;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.stereotype.Component;

@Component
public class XXX implements BeanFactoryPostProcessor {
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		/*
		* private final Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>();
		* you can get this property
		* */

		GenericBeanDefinition x = (GenericBeanDefinition) beanFactory.getBeanDefinition("x");
		x.setBeanClass(Y.class);
	}
}
