package com.spring.hello.world.customconfig;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class MyNamespaceHandler extends NamespaceHandlerSupport {
	@Override
	public void init() {
		registerBeanDefinitionParser("custom", new UserBeanDefinitionParser());
	}
}
