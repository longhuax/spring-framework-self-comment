package com.spring.hello.world.aopdemo.proxyDemo1;

public class ProxyUserDao implements publicInterface {
	private UserDao userDao = new UserDao();

	@Override
	public void save() {
		System.out.println("proxy begins");
		userDao.save();
		System.out.println("proxy ends");
	}

	@Override
	public void find() {
		System.out.println("proxy begins");
		userDao.find();
		System.out.println("proxy ends");

	}
}
