package com.spring.hello.world.aopdemo.proxyDemo1;


import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CGLibProxyInterceptor implements MethodInterceptor {
	@Override
	public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
		System.out.println("CGLib Dynamic Proxy begins");
		Object object = methodProxy.invokeSuper(o, objects);
		System.out.println("CGLib Dynamic Proxy ends");
		return object;
	}
}
