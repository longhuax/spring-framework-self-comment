package com.spring.hello.world;

import com.spring.hello.world.bean.denpendencies.BeanA;
import com.spring.hello.world.bean.denpendencies.CircleBeanPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class DependenciesTestApplication {
	public static void main(String[] args) {
		Resource rs = new ClassPathResource("dependenciesTest.xml");
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		new XmlBeanDefinitionReader(beanFactory).loadBeanDefinitions(rs);

		beanFactory.addBeanPostProcessor(new CircleBeanPostProcessor());
		BeanA beanA = (BeanA) beanFactory.getBean("beanA");

		System.out.println(beanA.toString());
	}

}
