package com.spring.hello.world.aopdemo.proxyDemo1;

public class CGLibProxy {
	public CGLibProxy(){
		System.out.println("CGLibProxy construct method");
	}

	public void hello() {
		System.out.println("CGLib Dynamic proxy method hello()");
	}

	final public String SayOthers(String name) {
		System.out.println("CGLib SayOthers >> " + name);
		return null;
	}
}
