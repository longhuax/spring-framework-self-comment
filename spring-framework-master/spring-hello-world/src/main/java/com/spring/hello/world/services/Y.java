package com.spring.hello.world.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component  // comment when test BeanFactoryPostProcessor
public class Y {
//	@Autowired  // comment when test BeanFactoryPostProcessor
	public X x;

	public Y() {
		System.out.println("Y created");
	}

	public void setX(X x) {
		this.x = x;
	}
}
