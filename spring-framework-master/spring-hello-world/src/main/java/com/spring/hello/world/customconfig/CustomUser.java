package com.spring.hello.world.customconfig;

/**
 * @author monco
 * @data 2020/9/27 15:28
 * @description :自定义需要注入到spring的对象
 */
public class CustomUser {

	private String id;

	private String username;

	private String email;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
