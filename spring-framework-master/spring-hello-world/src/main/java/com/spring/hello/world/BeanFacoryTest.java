package com.spring.hello.world;

import com.spring.hello.world.messageexample.MessageServiceImpl;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
//import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class BeanFacoryTest {

	public static void main(String[] args) {
//		before version3.0
//		BeanFactory bf = new XmlBeanFactory(new ClassPathResource("application.xml"));
//		MessageServiceImpl messageService = (MessageServiceImpl)bf.getBean("messageService");

//		after version3.0
		Resource rs = new ClassPathResource("application.xml");;
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		new XmlBeanDefinitionReader(beanFactory).loadBeanDefinitions(rs);
		MessageServiceImpl messageService = (MessageServiceImpl)beanFactory.getBean("messageService");

		System.out.println(messageService.getMessage());
	}
}
