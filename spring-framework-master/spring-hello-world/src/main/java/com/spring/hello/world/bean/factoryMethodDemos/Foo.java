package com.spring.hello.world.bean.factoryMethodDemos;

public class Foo {
	public Foo() {
		System.out.println("Foo created");
	}

	@Override
	public String toString() {
		return "Foo instance";
	}
}
