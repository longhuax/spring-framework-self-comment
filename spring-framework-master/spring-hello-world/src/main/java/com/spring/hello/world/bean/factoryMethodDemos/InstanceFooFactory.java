package com.spring.hello.world.bean.factoryMethodDemos;

public class InstanceFooFactory {
	public Foo createInstance() {
		return new Foo();
	}
}
