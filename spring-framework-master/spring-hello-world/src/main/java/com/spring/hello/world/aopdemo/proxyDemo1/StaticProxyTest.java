package com.spring.hello.world.aopdemo.proxyDemo1;

public class StaticProxyTest {
	public static void main(String[] args) {
		UserDao userDao = new UserDao();
		userDao.save();
		System.out.println("------------------");
		userDao.find();
		System.out.println("*********************");
		publicInterface proxyUserDao = new ProxyUserDao();
		proxyUserDao.save();
		System.out.println("------------------");
		proxyUserDao.find();
	}
}
