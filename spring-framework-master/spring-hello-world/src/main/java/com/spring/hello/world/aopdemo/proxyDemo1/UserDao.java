package com.spring.hello.world.aopdemo.proxyDemo1;

public class UserDao implements publicInterface {
	@Override
	public void save() {
		System.out.println("save user");
	}

	@Override
	public void find() {
		System.out.println("find user");
	}
}
