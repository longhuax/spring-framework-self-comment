package com.spring.hello.world.bean.xmlparser;

public abstract class GetBeanTest {
	public void showMe() {
		this.getBean().showMe();
	}

	public abstract User getBean();
}
