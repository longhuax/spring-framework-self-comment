package com.spring.hello.world.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/*
* 包名
* classpath \\targe\\class
* File file = new File();
* file.list();
* Class class = Class.forName();
* list.add(class)
* */
@Configuration
@ComponentScan("com.spring.hello.world")
public class App {
}
