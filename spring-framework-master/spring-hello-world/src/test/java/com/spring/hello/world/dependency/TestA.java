package com.spring.hello.world.dependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestA {
	@Autowired
	private TestB testB;

	public TestB getTestB() {
		return testB;
	}
}
