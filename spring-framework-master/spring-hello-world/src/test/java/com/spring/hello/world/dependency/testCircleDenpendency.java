package com.spring.hello.world.dependency;

import com.spring.hello.world.app.App;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class testCircleDenpendency {
	public static void main(String[] args) {
//	@Test
//	public void test() {
		AnnotationConfigApplicationContext c = new AnnotationConfigApplicationContext(App.class);
		TestA a = c.getBean(TestA.class);
		TestB b = c.getBean(TestB.class);
		System.out.println(a);
		System.out.println(b);
	}
}
