package com.spring.hello.world.dependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TestB {
	@Autowired
	private TestA testA;

	public TestA getTestA() {
		return testA;
	}
}
