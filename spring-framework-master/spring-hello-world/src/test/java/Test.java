import com.spring.hello.world.services.X;
import com.spring.hello.world.services.Y;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.spring.hello.world.app.App;

import java.util.List;

public class Test {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext a = new AnnotationConfigApplicationContext(App.class);
//		a.scan("");
		/*
		* Spring bean 循环依赖
		* Spring bean 是不是new
		* Spring bean 生命周期
		* */
//		System.out.println(a.getBean(X.class));
		System.out.println(a.getBean("x"));  // change x initial  by BeanFactoryPostProcessor

		/*
		* Java 对象循环依赖
		* */
//		X x = new X();
//		Y y = new Y();
//		x.setY(y);
//		y.setX(x);

		// spring 实例化过程
//		List<Class> list = null;
		/*
		* Class 作用
		*
		* 程序员------------抽象能力  最终能力
		* examples:
		* Person {
		* 	age name sex walk run
		* }
		* 电商
		* order {
		* }
		* product {
		* }
		* 描述世界中的类
		* Class {
		* 	Field[]
		* 	construct
		* 	Method
		* 	getSimpleName
		* }
		*
		*
		*
		* */
//		for(Class aclass : list) {
//			// spring 建模类
//			// BeanDefinition is a interface
//			GenericBeanDefinition genericBeanDefinition= new GenericBeanDefinition();
//			genericBeanDefinition.setLazyInit(false);
//			genericBeanDefinition.setBeanClass(aclass);
//			genericBeanDefinition.setAutowireMode(1);
//			genericBeanDefinition.setScope("singleton"); // or prototype only instance when referenced
//			genericBeanDefinition.setDescription("what is used for");
//			genericBeanDefinition.setDestroyMethodName("XXX");
//
//			//map.put("test", genericBeanDefinition); // beanname and beandefinition map
//			//map.put("x", genericBeanDefinition);
//		}

		/*
		* BeanfactoryPostProcessor
		* 先内部 后 自定义的
		* 后置处理器
		* 干预初始化过程 （不是实例化）
		* 实例化包含初始化
		* */

	}
}
