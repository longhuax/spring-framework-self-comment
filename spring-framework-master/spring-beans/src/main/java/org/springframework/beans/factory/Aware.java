/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory;

/**
 * A marker superinterface indicating that a bean is eligible to be notified by the
 * Spring container of a particular framework object through a callback-style method.
 * The actual method signature is determined by individual subinterfaces but should
 * typically consist of just one void-returning method that accepts a single argument.
 *
 * <p>Note that merely implementing {@link Aware} provides no default functionality.
 * Rather, processing must be done explicitly, for example in a
 * {@link org.springframework.beans.factory.config.BeanPostProcessor}.
 * Refer to {@link org.springframework.context.support.ApplicationContextAwareProcessor}
 * for an example of processing specific {@code *Aware} interface callbacks.
 *
 * @author Chris Beams
 * @author Juergen Hoeller
 * @since 3.1
 *
 * 这是一个用于标记的父接口。
 * 他通过一个回调方法来接收一个来自于特定框架对象的spring容器的通知。
 * 其实际的方法签名由具体的子接口确定，但通常只由一个接受单个参数的void返回方法组成。
 *
 * Aware这个单词本意是对...有兴趣的，自觉的。
 * 我看到网络上也有人将他翻译为自动的，含义都差不多。
 * 所以我们意译一下就知道，spring会自动的或主动的执行Aware接口实现类的方法。
 *
 * 例如：BeanNameAware,就是bean实例化完成之后，
 * spring主动调用BeanNameAware的setBeanName方法，
 * 向BeanNameAware通知当前类的beanName。
 *
 * 这样来看，Aware接口其实类似于另一种形式的自动注入。
 * 只不过这个自动注入的内容没有办法通过传统的@Autowired方式进行。
 * 实现不同的Aware子接口其实就是声明了当前类需要自动注入那种类型的信息。
 *
 */
public interface Aware {

}
