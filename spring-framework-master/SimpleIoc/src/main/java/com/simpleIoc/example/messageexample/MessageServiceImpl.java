package com.simpleIoc.example.messageexample;

public class MessageServiceImpl implements MessageService {
	public MessageServiceImpl() {
		System.out.println("MessageServiceImpl created");
	}
	public String getMessage() {
		return "hello world";
	}
}
